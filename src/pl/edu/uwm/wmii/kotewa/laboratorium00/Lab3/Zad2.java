package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab3;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;

public class Zad2 {
    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc) {
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            tab[i] = random.nextInt(maxWartosc - minWartosc) + minWartosc;
        }
    }

    public static int ileNieparzystych(int[] tab, int n){
        int nieparzyste = 0;
        for(int i = 0; i < n; i++)
            if (tab[i] % 2 == 1 || tab[i] % 2 == -1) nieparzyste++;
        return nieparzyste;
    }

    public static int ileparzystych(int[] tab, int n){
        int parzyste = 0;
        for(int i = 0; i < n; i++)
            if (tab[i] % 2 == 0) parzyste++;
        return parzyste;
    }

    public static int ileDodatnich(int[] tab, int n){
        int ile = 0;
        for(int i = 0; i < n ; i++)
            if(tab[i] > 0) ile++;
        return ile;
    }

    public static int ileUjemnych(int[] tab, int n){
        int ile = 0;
        for(int i = 0; i < n ; i++)
            if(tab[i] < 0) ile++;
        return ile;
    }

    public static int ileZerowych(int[] tab, int n){
        int ile = 0;
        for(int i = 0; i < n ; i++)
            if(tab[i] == 0) ile++;
        return ile;
    }

    public static int ileMaxymalnych(int[] tab, int n){
        int najwieksza = -999;
        int ilosc = 0;
        for(int i=0; i < n; i++){
            if(tab[i] > najwieksza) {
                najwieksza = tab[i];
                ilosc = 0;
            }
            if(tab[i] == najwieksza){
                ilosc++;
            }
        }
        return ilosc;
    }

    public static int sumaDodatnich(int[] tab, int n){
        int sumaDodatnich = 0;
        for(int i=0; i<n; i++) if (tab[i] > 0) sumaDodatnich += tab[i];
        return sumaDodatnich;
    }

    public static int sumaUjemnych(int[] tab, int n){
        int suma = 0;
        for(int i=0; i<n; i++) if (tab[i] < 0) suma += tab[i];
        return suma;
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich (int[] tab, int n){
        int dlugosc = 0;
        int najdluzszy = 0;
        for(int i = 0; i < n; i++){
            if (tab[i] > 0){
                dlugosc++;
                if (dlugosc >= najdluzszy){
                    najdluzszy = dlugosc;
                }
            }else{
                dlugosc = 0;
            }
        }
        return najdluzszy;
    }

    public static void signum(int[] tab, int n){
        for(int i=0; i<n; i++){
            if(tab[i] >= 0){
                tab[i] = 1;
            }else{
                tab[i] = -1;
            }
        }
    }

    public static void odwrocFragment(int[] tab, int lewy, int prawy){
        int temp;

        for(int i = lewy-1; i < prawy / 2; i++){
            temp = tab[i];
            tab[i] = tab[prawy-i];
            tab[prawy-i] = temp;
        }
    }


    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe: ");
        int n = scanner.nextInt();
        while(n <= 1 || n >=100){
            System.out.println("Liczba musi byc z zakresu 1-100, podaj jeszcze raz: ");
            n = scanner.nextInt();
        }
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);

        System.out.println(Arrays.toString(tab));

        System.out.printf("Nieparzystych: %d\n Parzystych: %d\n Dodatnich: %d\n Ujemnych: %d\n Zerowych: %d\n" +
                " Maxymalnych: %d\n Suma dodatnich: %d\n Suma ujemnych: %d\n Dlugosc maxymalnego ciagu dodatnich: %d\n",
                ileNieparzystych(tab, n), ileparzystych(tab, n), ileDodatnich(tab, n), ileUjemnych(tab, n),
                ileZerowych(tab, n), ileMaxymalnych(tab, n), sumaDodatnich(tab, n), sumaUjemnych(tab, n),
                dlugoscMaksymalnegoCiaguDodatnich(tab, n));



    }
}
