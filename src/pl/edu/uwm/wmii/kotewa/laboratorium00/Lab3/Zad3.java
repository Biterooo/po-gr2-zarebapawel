package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab3;

import java.util.Scanner;
import java.util.Random;

public class Zad3 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj m: ");
        int m = scanner.nextInt();
        System.out.println("Podaj n: ");
        int n = scanner.nextInt();
        System.out.println("Podaj k: ");
        int k = scanner.nextInt();

        while(m < 1 || m > 10){
            System.out.println("Wartosc m musi byc z przedzialu 1-10, Podaj jeszcze raz m: ");
            m = scanner.nextInt();
        }
        while(n < 1 || n > 10){
            System.out.println("Wartosc n musi byc z przedzialu 1-10, Podaj jeszcze raz n: ");
            n = scanner.nextInt();
        }
        while(k < 1 || k > 10){
            System.out.println("Wartosc k musi byc z przedzialu 1-10, Podaj jeszcze raz k: ");
            k = scanner.nextInt();
        }

        int[][] a = new int[m][n];
        int[][] b = new int[n][k];
        Random random = new Random();
        for(int i=0; i < m; i++){
            for(int j = 0; j < n; j++){
                a[i][j] = random.nextInt(1998)-999;
            }
        }
        for(int i=0; i < n; i++){
            for(int j = 0; j < k; j++){
                b[i][j] = random.nextInt(1998)-999;
            }
        }

        for(int i=0; i < m; i++){
            System.out.print("[");
            for(int j = 0; j < n; j++){
                System.out.print(a[i][j] + ",");
            }
            System.out.println("]");
        }
        System.out.println();

        for(int i=0; i < n; i++){
            System.out.print("[");
            for(int j = 0; j < k; j++){
                System.out.print(b[i][j] + ",");
            }
            System.out.println("]");
        }
        System.out.println();

        int[][] c = new int[n][k];

        if(m == k){
            for(int wie=0; wie < m; wie++){
                for(int kol=0; kol < k; kol++){
                    c[wie][kol] += a[wie][kol] * b[kol][wie];
                }
            }
            for(int i=0; i < m; i++){
                System.out.print("[");
                for(int j = 0; j < k; j++){
                    System.out.print(c[i][j] + ",");
                }
                System.out.println("]");
            }
        }else{
            System.out.println("Mnozenie nie jest mozliwe, poniewaz liczba kolumn 'a' musi sie rownac liczbie wierszy 'b'");
        }
        
    }
}
