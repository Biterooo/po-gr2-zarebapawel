package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab3;
import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;


public class Zad1 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int n = 101;
        while(n < 1 || n > 100 ){
            System.out.println("Podaj liczbe z zakresu 1-100:");
            n = scanner.nextInt();
        }
        int[] tab = new int[n];
        Random losowe = new Random();
        for(int i = 0; i < n; i++){
            tab[i] = losowe.nextInt(1998)-999;
        }
        System.out.println(Arrays.toString(tab));

        //a
        int parzyste = 0;
        int nieparzyste = 0;
        for(int i = 0; i < n; i++){
            if(tab[i] % 2 == 0){
                parzyste++;
            } else {
                nieparzyste++;
            }
        }
        System.out.printf("Ilosc parzystych: %d, ilosc nieparzystych: %d\n", parzyste, nieparzyste);

        //b
        int ujemne = 0;
        int zero = 0;
        int dodatnie = 0;
        for(int i = 0; i < n; i++){
            if(tab[i] < 0){
                ujemne++;
            } else if(tab[i] > 0){
                dodatnie++;
            } else{
                zero++;
            }
        }
        System.out.printf("Ilosc ujemnych: %d, ilosc zer: %d, ilosc dodatnich: %d\n", ujemne, zero, dodatnie);

        //c
        int najwieksza = -999;
        int ilosc = 0;
        for(int i=0; i < n; i++){
            if(najwieksza > tab[i]) {
                najwieksza = tab[i];
                ilosc = 0;
            }
            if(tab[i] == najwieksza){
                ilosc++;
            }
        }
        System.out.printf("ilosc najwiekszych: %d\n", ilosc);

        //d
        int sumaDodatnich = 0;
        int sumaUjemnych = 0;
        for(int i=0; i<n; i++){
            if (tab[i] >= 0) sumaDodatnich++;
            else{
                sumaUjemnych++;
            }
        }
        System.out.println("Suma dodatnich: " + sumaDodatnich + ", Suma ujemnych: " + sumaUjemnych);

        //e
        int dlugosc = 0;
        int najdluzszy = 0;
        for(int i=0; i<n; i++){
            if (tab[i] > 0){
                dlugosc++;
                if (dlugosc >= najdluzszy){
                    najdluzszy = dlugosc;
                }
            }else{
                dlugosc = 0;
            }
        }
        System.out.printf("Najdluzszy fragment dodatni tablicy: %d\n", najdluzszy);

        //f
        for(int i=0; i<n; i++){
            if(tab[i] >= 0){
                tab[i] = 1;
            }else{
                tab[i] = -1;
            }
        }

        //g
        System.out.println("Podaj liczbe 1: ");
        int lewy = scanner.nextInt();
        System.out.println("Podaj liczbe 2 (musi byc wieksza od 1 liczby): ");
        int prawy = scanner.nextInt();
        int temp;

        for(int i = lewy-1; i < prawy / 2; i++){
            temp = tab[i];
            tab[i] = tab[prawy-i];
            tab[prawy-i] = temp;
        }

        System.out.println(Arrays.toString(tab));
    }


}