package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab6;


public class IntegerSet {
    boolean[] set = new boolean[100];


    public static boolean[] union(boolean[] s1 , boolean[] s2) {
        boolean[] nowa = new boolean[100];

        for(int i = 0; i < 100; i++){
            nowa[i] = s1[i] || s2[i];
        }
        return nowa;
    }

    public static boolean[] intersection(boolean[] s1, boolean[] s2) {
        boolean[] nowa = new boolean[100];

        for(int i = 0; i < 100; i++){
            nowa[i] = s1[i] && s2[i];
        }
        return nowa;
    }
    public void insertElement(int liczba){
        if(liczba >= 1 && liczba <=100)
            set[liczba - 1] = true;
    }

    public void deleteElement(int liczba){
        if(liczba >= 1 && liczba <=100)
            set[liczba - 1] = false;
    }

    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < 100; i++){
            if (set[i]){
                str.append(i + 1).append(" ");
            }
        }
        return str.toString();

    }

    public boolean equals(boolean[] zbior){
        for (int i = 0; i < 100; i ++){
            if(set[i] != zbior[i]){
                return false;
            }
        }
        return true;
    }

}
