package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab6;



public class zad2 {
    public static void main(String[] args){
        IntegerSet o1 = new IntegerSet();
        IntegerSet o2 = new IntegerSet();
        IntegerSet o3 = new IntegerSet();

        for(int i = 0; i < 100; i = i + 3){
            o1.insertElement(i);
            o3.insertElement(i);
        }

        for (int i = 0; i < 100; i += 5){
            o2.insertElement(i);
        }

        System.out.println("reprezentacja o1: " + o1);
        System.out.println("reprezentacja o2: " + o2);
        System.out.println("reprezentacja o3: " + o3);
        System.out.println("czy o1 jest rowny o2: " + o1.equals(o2.set));
        System.out.println("czy o1 jest rowny o3: " + o1.equals(o3.set));
        o1.insertElement(50);
        System.out.println("czy o1 z dodatkowym 50 jest rowny o3: " + o1.equals(o3.set));
        o1.deleteElement(50);
        System.out.println("czy o1 bez 50 jest rowny o3: " + o1.equals(o3.set));
        System.out.println("Czy o1 jest rowny iloczynowi o1 i o3: "+ o1.equals(IntegerSet.intersection(o1.set, o3.set)));
        System.out.println("Czy o1 jest rowny sumie o1 i o2: "+ o1.equals(IntegerSet.union(o1.set, o2.set)));

    }
}
