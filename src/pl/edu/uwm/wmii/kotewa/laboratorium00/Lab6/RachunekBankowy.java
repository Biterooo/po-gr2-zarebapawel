package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab6;


public class RachunekBankowy {
    static double rocznaStopaProcentowa = 5;
    private double saldo;

    RachunekBankowy(double saldo){
        this.saldo = saldo;
    }

    public void obliczMiesieczneOdsetki(){
        double wynik = getSaldo() * rocznaStopaProcentowa / 12;
        this.setSaldo(getSaldo() + wynik);
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public static void setRocznaStopaProcentowa(double rocznaStopaProcentowa) {
        RachunekBankowy.rocznaStopaProcentowa = rocznaStopaProcentowa;
    }


}
