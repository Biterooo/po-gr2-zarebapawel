package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab7;

import pl.imiajd.zareba.Lab7.BetterRectangle;

public class ProgramTestujacyZad6 {
    public static void main(String[] args) {
        BetterRectangle betterRectangle = new BetterRectangle(0, 0, 3, 4);
        System.out.println(betterRectangle.getPerimeter());
        System.out.println(betterRectangle.getArea());
    }
}

