package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab7;

import pl.imiajd.zareba.Lab7.Nauczyciel;
import pl.imiajd.zareba.Lab7.Osoba;
import pl.imiajd.zareba.Lab7.Student;

public class ProgramTestujacyZad4 {
    public static void main(String[] args) {
        Osoba osoba = new Osoba("Mickiewicz", 1999);
        Student student = new Student("Sienkiewicz", 1990, "Informatyka");
        Nauczyciel nauczyciel = new Nauczyciel("Słowacki", 1995, 5000);

        System.out.println(osoba);
        System.out.println(student);
        System.out.println(nauczyciel);

        System.out.println(osoba.getNazwisko());
        System.out.println(student.getNazwisko());
        System.out.println(nauczyciel.getNazwisko());

        System.out.println(student.getKierunek());
        System.out.println(nauczyciel.getPensja());
    }

}

