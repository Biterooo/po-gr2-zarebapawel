package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab12;

import java.util.HashSet;
import java.util.Scanner;


public class Zad7 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Podaj liczbe: ");
        Integer n = sc.nextInt();

        sitoEratostenesa(n);
    }

    public static void sitoEratostenesa(Integer n){

        HashSet<Integer> pierwsze = new HashSet<>();
        for(int i = 2; i < n; i++){
            pierwsze.add(i);
        }

        for(int i = 2; i < Math.sqrt(pierwsze.size()); i++){
            for(int j = i; j < n; j += i){
                if(j != i){
                    pierwsze.remove(j);
                }
            }
        }

        for(Integer i : pierwsze){
            System.out.print(i + " ");
        }

    }
}
