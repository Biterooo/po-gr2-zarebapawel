package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab12;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Zad2 {
    public static void main(String[] args){
        LinkedList<String> pracownicy = new LinkedList<>(List.of("Kowalski", "Mickiewicz", "Sienkiewicz", "Słowacki",
                "Nowak", "Kowalczyk", "Pawlak", "Kargul"));

        System.out.println(Arrays.toString(pracownicy.toArray()));

        redukuj(pracownicy, 3);

        System.out.println(Arrays.toString(pracownicy.toArray()));
    }


    public static <T> void redukuj(LinkedList<T> pracownicy, int n) {
        Iterator<T> iter = pracownicy.iterator();
        int i = 0;
        while(iter.hasNext()){
            iter.next();
            if(i % n == n - 1){
                iter.remove();
            }
            i++;
        }


        //for(int i = n - 1; i < pracownicy.size(); i += n - 1){
        //  pracownicy.remove(i);
        //}
    }
}
