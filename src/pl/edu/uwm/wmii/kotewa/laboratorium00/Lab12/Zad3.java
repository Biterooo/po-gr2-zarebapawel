package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab12;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Zad3 {
    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<>(List.of("Kowalski", "Mickiewicz", "Sienkiewicz", "Słowacki",
                "Nowak", "Kowalczyk", "Pawlak", "Kargul"));

        System.out.println(Arrays.toString(pracownicy.toArray()));

        odwroc(pracownicy);

        System.out.println(Arrays.toString(pracownicy.toArray()));

    }

    public static void odwroc(LinkedList<String> lista) {
        LinkedList<String> temp = new LinkedList<>(lista);

        for(int i = lista.size() - 1, j = 0; i >= 0; i--, j++) {
            lista.set(j, temp.get(i));
        }


    }
}
