package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab12;

import java.util.Stack;

public class Zad5 {
    public static void main(String[] args){
        Stack<String> stos = new Stack<>();

        String zdanie = "Ala ma kota. Jej kot lubi myszy.";

        String[] podzielone = zdanie.split(" ");

        String wynik = "";

        for(int i = 0; i < podzielone.length; i++){
            if(!podzielone[i].endsWith(".")){
                podzielone[i] = Character.toLowerCase(podzielone[i].charAt(0)) + podzielone[i].substring(1);
                stos.push(podzielone[i]);
            } else {
                podzielone[i] = podzielone[i].substring(0, podzielone[i].length() - 1);
                stos.push(podzielone[i]);
                String temp = "";
                while(!stos.isEmpty()){
                    temp += stos.pop() + " ";

                }
                temp = Character.toUpperCase(temp.charAt(0)) + temp.substring(1);
                temp = temp.substring(0, temp.length() - 1);
                wynik += temp +  ". ";
            }

        }

        System.out.println(wynik);
    }
}
