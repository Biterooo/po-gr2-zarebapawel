package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab12;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Zad8 {
    public static void main(String[] args){
        LinkedList<String> pracownicy = new LinkedList<>(List.of("Kowalski", "Mickiewicz", "Sienkiewicz", "Słowacki",
                "Nowak", "Kowalczyk", "Pawlak", "Kargul"));
        print(pracownicy);
    }


    public static <E extends Iterable<?>> void print(E arg){
        for (Object o : arg) {
            System.out.print(o + ", ");
        }

    }
}
