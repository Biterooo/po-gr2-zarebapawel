package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab12;


import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class Zad6 {
    public static void main(String[] args) {
        Stack<Integer> stos = new Stack<>();
        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj liczbe: ");
        Integer liczba = sc.nextInt();
        while(liczba > 0){
            stos.push(liczba % 10);
            liczba /= 10;
        }

        while(!stos.isEmpty()){
            System.out.print(stos.pop() + " ");
        }


    }


}
