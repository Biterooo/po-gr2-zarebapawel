package pl.edu.uwm.wmii.kotewa.laboratorium00.Test2;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args){
        ObslugaKlienta ok = new ObslugaKlienta(new ArrayList<>());
        ok.getAl().add(new Klient("Mickiewicz", 1, LocalDate.of(1990, 8, 3), 500.5));
        ok.getAl().add(new Klient("Sienkiewicz", 3, LocalDate.of(1677, 5, 20), 1000));
        ok.getAl().add(new Klient("Mickiewicz", 15, LocalDate.of(1544, 3, 7), 150.3));
        ok.getAl().add(new Klient("Zar", 7, LocalDate.of(1990, 8, 3), 1000));
        ok.getAl().add(new Klient("Mickiewicz", 2, LocalDate.of(2020, 2, 2), 20.5));

        for(Klient i : ok.getAl()){
            System.out.println(i);
        }

        Collections.sort(ok.getAl());
        System.out.println();

        for(Klient i : ok.getAl()){
            System.out.println(i);
        }

        System.out.println();

        ObslugaKlienta.setProcentRabatu();
        System.out.println("kwota rabatu dla 2 klienta: ");
        System.out.println(ObslugaKlienta.discountAmount(ok.getAl().get(1)));

        HashMap<Integer, String> mapaRabatow = ObslugaKlienta.DiscountMap(ok.getAl());

        System.out.println();

        for(Map.Entry<Integer, String> o : mapaRabatow.entrySet()){
            System.out.println("id: " + o.getKey() + ".  nazwa, kwota rabatu: " + o.getValue());
        }

    }
}

class Klient implements Cloneable, Comparable<Klient> {
    private String nazwa;
    private int id;
    private LocalDate dataZakupy;
    private double rachunek;

    public Klient(String nazwa, int id, LocalDate dataZakupy, double rachunek) {
        this.nazwa = nazwa;
        this.id = id;
        this.dataZakupy = dataZakupy;
        this.rachunek = rachunek;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getId() {
        return id;
    }

    public LocalDate getDataZakupy() {
        return dataZakupy;
    }

    public double getRachunek() {
        return rachunek;
    }

    @Override
    public String toString() {
        return "Klient{" +
                "nazwa='" + nazwa + '\'' +
                ", id=" + id +
                ", dataZakupy=" + dataZakupy +
                ", rachunek=" + rachunek +
                '}';
    }

    @Override
    public int compareTo(Klient o) {
        if(dataZakupy.compareTo(o.getDataZakupy()) == 0) {
            if(nazwa.compareTo(o.getNazwa()) == 0){
                return Double.compare(rachunek, o.getRachunek());
            }
            return nazwa.compareTo(o.getNazwa());
        }
        return dataZakupy.compareTo(o.getDataZakupy());
    }
}

class ObslugaKlienta{
    static double procentRabatu;
    private ArrayList<Klient> al;

    public ObslugaKlienta(ArrayList<Klient> al) {
        this.al = al;
    }

    public static void setProcentRabatu() {
        ObslugaKlienta.procentRabatu = 0.05;
    }

    public ArrayList<Klient> getAl() {
        return al;
    }

    public static double discountAmount(Klient k){
        if(k.getRachunek() > 300){
            return k.getRachunek() * procentRabatu;
        }
        return 0;
    }

    public static HashMap<Integer, String> DiscountMap(ArrayList<Klient> klist){
        HashMap<Integer, String> mapa = new HashMap<>();
        for(Klient o : klist){
            if(discountAmount(o) > 0){
                String temp = o.getNazwa().concat(" ").concat(String.valueOf(discountAmount(o)));
                mapa.put(o.getId(), temp);
            }
        }
        return mapa;

    }
}