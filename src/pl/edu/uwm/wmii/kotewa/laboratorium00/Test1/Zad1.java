package pl.edu.uwm.wmii.kotewa.laboratorium00.Test1;

import java.util.*;

public class Zad1 {
    public static void main(String[] args) {
        //Wywolanie zadanie 2
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        ArrayList<Integer> b = new ArrayList<>(Arrays.asList(7, 8, 6, 10));
        System.out.println(merge(a, b));


        //Wywolanie zadanie 1
        podpunktA();

    }

    //zad1

    public static void podpunktA() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj Liczbe: ");
        int n = scanner.nextInt();


        for(int i = 0; i < n; i++) {
            System.out.println("Podaj Liczbe do sprawdzenia: ");
            int liczba = scanner.nextInt();
            boolean czyJest = true;

            for(int j = 2; j <= liczba / 2; ++j) {
                if (liczba % j == 0) {
                    czyJest = false;
                    break;
                }

                }
            if (!czyJest) {
                System.out.println(liczba);

            }

        }
    }


    //zad2
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> nowa = new ArrayList<>();

        for(int i = 0; i < a.size() || i < b.size(); i++){
            if(i < a.size()){
                nowa.add(a.get(i));
            }
            if(i < b.size()){
                nowa.add(b.get(i));
            }
        }
        Set<Integer> set = new HashSet<>(nowa);
        nowa.clear();
        nowa.addAll(set);

        return nowa;
    }


}
