package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab4;

import java.math.BigInteger;

public class zad4 {
    public static BigInteger func(int n){
        BigInteger bi = new BigInteger("1");
        BigInteger wynik = new BigInteger("0");

        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                wynik = wynik.add(bi);
                bi = bi.multiply(new BigInteger("2"));
            }
        }
        return wynik;


    }

    public static void main(String[] args){
        int n = 8;
        System.out.println(func(n));


    }
}
