package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab4;

import java.io.FileReader;


public class zad2 {
    public static int func(String nazwaPliku, char znak)throws Exception{
        FileReader plik = new FileReader("src\\pl\\edu\\uwm\\wmii\\kotewa\\laboratorium00\\Lab4\\" + nazwaPliku);

        int ile = 0;
        int i;

        while ((i=plik.read()) != -1){
            System.out.print((char) i);
            if((char)i == znak){
                ile++;
            }
        }
        return ile;
    }

    public static void main(String[] args)throws Exception {
        int ileZnakow = func("tekst.txt", 'a');
        System.out.println("\nilosc podanego znaku: "+ ileZnakow);
    }
}
