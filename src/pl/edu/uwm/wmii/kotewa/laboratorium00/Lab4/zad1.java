package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab4;


import java.util.Arrays;

public class zad1 {
    public static int countChar(String str, char c){
        int ile = 0;
        for (int i = 0; i < str.length(); i++){
            if(str.charAt(i) == c){
                ile++;
            }
        }
        return ile;
    }

    public static int countSubStr(String str, String subStr){
        int ile = 0;

        for(int i = 0; i <= str.length() - subStr.length(); i++){
            if(str.substring(i, i + subStr.length()).equals(subStr))
                ile++;
        }
        return ile;

    }

    public static String middle(String str){
        if(str.length() % 2 == 0){
            return str.substring(str.length() / 2 - 1, (str.length() / 2) + 1);
        }
        else{
            return str.substring(str.length() / 2, str.length() / 2 + 1);
        }
    }

    public static String repeat(String str, int n){
        String tekst = "";

        for(int i = 0; i < n; i++){
            tekst = tekst.concat(str);
        }
        return tekst;
    }

    public static int[] where(String str, String subStr) {
        int[] tab = new int[str.length()];
        int index = 0;

        for (int i = 0; i < str.length() - subStr.length(); i++) {
            if (str.substring(i, i + subStr.length()).equals(subStr)) {
                tab[index] = i + 1;
                index++;
            }
        }
        return tab;
    }

    public static String change(String str){
        char[] tab = new char[str.length()];

        for(int i = 0; i < str.length(); i++){
            tab[i] = str.charAt(i);
        }

        char[] tab2 = new char[str.length()];
        StringBuffer temp = new StringBuffer(str.length());

        for(int i = 0; i < str.length(); i++){
            if(Character.isUpperCase(tab[i])){
                tab2[i] = Character.toLowerCase(tab[i]);
            }else{
                tab2[i] = Character.toUpperCase(tab[i]);
            }
            temp.append(tab2[i]);
        }

        return temp.toString();
    }

    public static String nice(String str){
        StringBuffer temp = new StringBuffer(str.length());

        for(int i = 0; i < str.length(); i++){
            temp.append(str.charAt(i));
        }

        int licz = 0;
        for(int i = str.length(); i > 0; i--){
            if(licz == 3){
                temp.insert(i, '\'');
                licz = 0;
            }
            licz++;
        }
        return temp.toString();
    }

    public static String nice(String str, char sep, int ile){
        StringBuffer temp = new StringBuffer(str.length());

        for(int i = 0; i < str.length(); i++){
            temp.append(str.charAt(i));
        }

        int licz = 0;
        for(int i = str.length(); i > 0; i--){
            if(licz == ile){
                temp.insert(i, sep);
                licz = 0;
            }
            licz++;
        }
        return temp.toString();
    }

    public static void main(String[] args){
        String napis1 = "Ala ma kota a kot ma Ale";
        String napis2 = "ot";

        System.out.println("podpunkt a: "+ countChar(napis1, 'a'));
        System.out.println("podpunkt b: "+ countSubStr(napis1, napis2));
        System.out.println("podpunkt c: "+ middle("midkdle"));
        System.out.println("podpunkt d: "+ repeat("ho", 5));
        System.out.println("podpunkt e: "+ Arrays.toString(where(napis1, napis2)));
        System.out.println("podpunkt f: "+ change(napis1));
        System.out.println("podpunkt g: "+ nice("32312321"));
        System.out.println("podpunkt h: "+ nice("32312321", '.', 2));
    }
}
