package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab13;

import java.util.*;

public class Zad2{
    private static Map<String, String> mapa = new TreeMap<>(new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            return o1.compareTo(o2);
        }
    });

    public static void main(String[] args){
        program();
    }

    private static void dodaj(String nazwisko, String ocena){
        mapa.put(nazwisko, ocena);
    }

    private static void usun(String nazwisko){
        mapa.remove(nazwisko);
    }

    private static void wyswietl(){
        System.out.println("---------LISTA---------");
        for(Map.Entry<String, String> o : mapa.entrySet()){
            System.out.println(o.getKey() + ": " + o.getValue() );
        }
        System.out.println("--------------------------");
    }



    public static void program(){
        Scanner sc = new Scanner(System.in);

        while(true) {
            System.out.println("________________________");
            System.out.println("1. 'dodaj'.");
            System.out.println("2. 'usun'.");
            System.out.println("3. 'wyswietl'.");
            System.out.println("4. 'zmien'.");
            System.out.println("5. 'zakoncz'.");
            System.out.print("Wpisz jedno z polecen:  ");
            String polecenie = sc.nextLine();

            if(polecenie.equals("dodaj")){
                System.out.print("Podaj nazwisko do dodania: ");
                String nazwisko = sc.nextLine();
                System.out.print("Podaj ocene: ");
                String ocena = sc.nextLine();
                dodaj(nazwisko, ocena);
            }

            if(polecenie.equals("usun")){
                System.out.print("Podaj nazwisko do usuneicia: ");
                String nazwisko = sc.nextLine();
                usun(nazwisko);
            }

            if(polecenie.equals("wyswietl")){
                wyswietl();
            }

            if(polecenie.equals("zmien")){
                System.out.print("Podaj nazwisko do zmiany: ");
                String nazwisko = sc.nextLine();
                System.out.print("Podaj na jaka ocene zmienic: ");
                String ocena = sc.nextLine();
                dodaj(nazwisko, ocena);
            }

            if(polecenie.equals("zakoncz")){
                break;
            }

        }


    }
}
