package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab13;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class ListTODO{
    private static Queue<Zadanie> pq = new PriorityQueue<>();

    public static void main(String[] args){
        program();
    }

    private static void dodajZadanie(Zadanie zadanie){
        pq.add(zadanie);
    }

    private static void usunZadanie(){
        pq.remove();
    }

    private static void wyswietlZadania(){
        System.out.println("---------LISTA---------");
        for(Zadanie zad : pq){
            System.out.println("|" + zad.getProrytet() + ". " + zad.getOpis() + "|");
        }
        System.out.println("--------------------------");
    }


    public static void program(){
        Scanner sc = new Scanner(System.in);

        while(true) {
            System.out.println("1. 'Dodaj *priorytet* *opis*'.");
            System.out.println("2. 'nastepne'.");
            System.out.println("3. 'pokaz'.");
            System.out.println("4. 'zakoncz'.");
            System.out.print("Wpisz jedno z polecen:  ");
            String polecenie = sc.nextLine();

            if(polecenie.substring(0, 5).equals("Dodaj")){
                dodajZadanie(new Zadanie(Integer.parseInt(polecenie.substring(6, 7)), polecenie.substring(8)));
            }

            if(polecenie.equals("nastepne")){
                usunZadanie();
            }

            if(polecenie.equals("pokaz")){
                wyswietlZadania();
            }

            if(polecenie.equals("zakoncz")){
                break;
            }


        }
    }

}

class Zadanie implements Comparable<Zadanie> {
    private final Integer priorytet;
    private final String opis;

    Zadanie(Integer priorytet, String opis) {
        this.priorytet = priorytet;
        this.opis = opis;
    }

    public Integer getProrytet(){
        return priorytet;
    }

    public String getOpis(){
        return opis;
    }

    @Override
    public int compareTo(Zadanie o) {
        return this.priorytet.compareTo(o.priorytet);
    }
}
