package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab11;

import java.time.LocalDate;
import java.util.Arrays;


public class ArrayUtilDemoZad3 {
    public static void main(String[] args) {
        Integer[] tab = {10, 12, 6, 2 ,5, 3};

        System.out.println(Arrays.asList(tab));
        System.out.println("Czy jest uporządkowana niemalejaco: " + ArrayUtil.isSorted(tab));
        System.out.println();

        Integer[] tabSorted = {2, 4, 6, 8, 10, 12};

        System.out.println(Arrays.asList(tabSorted));
        System.out.println("Czy jest uporządkowana niemalejaco: " + ArrayUtil.isSorted(tabSorted));
        System.out.println();

        LocalDate[] daty = new LocalDate[]{LocalDate.of(1299, 1, 25),
                LocalDate.of(1990, 5, 3),
                LocalDate.of(1756, 4, 11),
                LocalDate.of(1939, 6, 2),
                LocalDate.of(1987, 4, 30)};

        System.out.println(Arrays.asList(daty));
        System.out.println("Czy jest uporządkowana niemalejaco: " + ArrayUtil.isSorted(daty));
        System.out.println();

        LocalDate[] datySorted = new LocalDate[]{LocalDate.of(1499, 1, 25),
                LocalDate.of(1590, 5, 3),
                LocalDate.of(1656, 4, 11),
                LocalDate.of(1739, 6, 2),
                LocalDate.of(1887, 4, 30)};

        System.out.println(Arrays.asList(datySorted));
        System.out.println("Czy jest uporządkowana niemalejaco: " + ArrayUtil.isSorted(datySorted));




    }
}
