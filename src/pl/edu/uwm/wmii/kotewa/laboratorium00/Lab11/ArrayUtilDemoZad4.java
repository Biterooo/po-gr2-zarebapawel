package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab11;

import java.time.LocalDate;

public class ArrayUtilDemoZad4 {
    public static void main(String[] args) {
        Integer[] tab = new Integer[]{2, 4, 5, 7, 8, 10, 12, 20, 30, 50, 80, 120, 300, 700, 1200, 3000, 10000, 12000};
        System.out.println("Index 1200 w tab: " + ArrayUtil.binSearch(tab, 1200));


        LocalDate[] datyTab = new LocalDate[]{LocalDate.of(1499, 1, 25),
                LocalDate.of(1590, 5, 3),
                LocalDate.of(1656, 4, 11),
                LocalDate.of(1739, 6, 2),
                LocalDate.of(1887, 4, 30)};

        System.out.println("Index daty 1739-6-2 w datyTab: " + ArrayUtil.binSearch(datyTab, LocalDate.of(1739, 6, 2)));
    }
}
