package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab11;

import java.util.Arrays;

public class ArrayUtil {

    public static <T extends Comparable<T>> boolean isSorted(T[] tab) {
        for (int i = 0; i < tab.length - 1; i++) {
            if(tab[i].compareTo(tab[i + 1]) >= 0){
                return false;
            }
        }
        return true;
    }

    public static <T extends Comparable<T>> int binSearch(T[] tab, T szukanyOb) {
        int left = 0;
        int right = tab.length;

        while (left <= right){
            int srodek = left + (right - left) / 2;

            if(tab[srodek].compareTo(szukanyOb) == 0){
                return srodek;
            }
            if(tab[srodek].compareTo(szukanyOb) < 0){
                left = srodek + 1;
            }
            if(tab[srodek].compareTo(szukanyOb) > 0){
                right = srodek - 1;
            }
        }
        return -1;
    }

    public static <T extends Comparable<T>> void selectionSort(T[] tab) {
        for(int i = 0; i < tab.length - 1; i++) {
            int minIndex = i;

            for(int j = i + 1; j < tab.length; j++) {
                if (tab[minIndex].compareTo(tab[j]) > 0) {
                    minIndex = j;
                }
            }

            T temp = tab[i];
            tab[i] = tab[minIndex];
            tab[minIndex] = temp;
        }
    }

    public static <T extends Comparable<T>> void mergeSort(T[] tab) {
        if(tab.length <= 1) return;

        int srodek = tab.length / 2;

        T[] left = Arrays.copyOfRange(tab, 0, srodek);

        T[] right = Arrays.copyOfRange(tab, srodek, tab.length);


        mergeSort(left);
        mergeSort(right);

        int i = 0, j = 0, k = 0;

        while(i < left.length && j < right.length) {
            if(left[i].compareTo(right[j]) < 0) {
                tab[k] = left[i];
                i++;
            } else {
                tab[k] = right[j];
                j++;
            }
            k++;
        }

        while(i < left.length) {
            tab[k] = left[i];
            i++;
            k++;
        }

        while(j < right.length) {
            tab[k] = right[j];
            j++;
            k++;
        }

    }

}
