package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab11;

import java.time.LocalDate;
import java.util.Arrays;

public class ArrayUtilDemoZad5 {
    public static void main(String[] args) {
        Integer[] tab = {10, 120, 6, 2 ,5, 3, 12, -8, 3, 50, 6};

        System.out.println("Przed sortowaniem: " + Arrays.asList(tab));

        ArrayUtil.selectionSort(tab);

        System.out.println("Po sortowaniu: " + Arrays.asList(tab));
        System.out.println();

        LocalDate[] daty = new LocalDate[]{LocalDate.of(1299, 1, 25),
                LocalDate.of(1990, 5, 3),
                LocalDate.of(1756, 4, 11),
                LocalDate.of(1839, 6, 2),
                LocalDate.of(1990, 4, 30)};


        System.out.println("daty przed sortowaniem: " + Arrays.asList(daty));

        ArrayUtil.selectionSort(daty);

        System.out.println("daty po sortowaniu: " + Arrays.asList(daty));

    }
}
