package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab2;

import java.util.Scanner;

public class Zad2p2 {
    public static void func(int n){
        Scanner scanner = new Scanner(System.in);
        float wczytane[] = new float[n];

        for(int i = 0; i < n; i++){
            System.out.println("Podaj Liczbe: ");
            wczytane[i] = scanner.nextFloat();

        }
        int suma = 0;
        for(int i = 0; i < n; i++){
            if(wczytane[i] > 0){
                suma += wczytane[i] * 2;
            }

        }
        System.out.println(suma);


    }
    public static void main(String[] args){
        System.out.println("Podaj ilosc: ");
        Scanner scanner = new Scanner(System.in);
        int ilosc = scanner.nextInt();
        func(ilosc);

    }
}
