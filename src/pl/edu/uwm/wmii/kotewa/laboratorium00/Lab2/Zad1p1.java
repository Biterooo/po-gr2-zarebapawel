package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab2;

import java.util.Scanner;
import static java.lang.Math.*;

public class Zad1p1 {

    public static int a(int n) {
        Scanner scanner = new Scanner(System.in);
        int x = 0;

        for (int i = 1; i <= n; i++) {
            System.out.println("Podaj liczbe: ");
            int podanaLiczba = scanner.nextInt();
            x += podanaLiczba;
        }
        return x;
    }

    public static float b(int n) {
        Scanner scanner = new Scanner(System.in);
        float x = 1;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            float b = scanner.nextFloat();
            x *= b;
        }
        return x;
    }
    public static float c(int n) {
        Scanner scanner = new Scanner(System.in);
        float x = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            float b = scanner.nextFloat();
            if (b < 0){
                b *= (-1);
            }
            x += b;
        }
        return x;
    }
    public static double d(int n) {
        Scanner scanner = new Scanner(System.in);
        double a = 0;
        double c = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            double b = scanner.nextDouble();
            if (b < 0){
                b *= (-1);
            }
            c = Math.sqrt(b);
            a += c;
        }
        return c;
    }
    public static float e(int n) {
        Scanner scanner = new Scanner(System.in);
        float a = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            float b = scanner.nextFloat();
            if (b < 0){
                b *= (-1);
            }
            a *= b;
        }
        return a;
    }
    public static float f(int n) {
        Scanner scanner = new Scanner(System.in);
        float a = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            float b = scanner.nextFloat();
            b *=b;
            a += b;
        }
        return a;
    }
    public static void g(int n) {
        Scanner scanner = new Scanner(System.in);
        float s = 0;
        float il = 1;
        float b = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe: ");
            b = scanner.nextFloat();
            s += b;
            il *= b;
        }
        System.out.println("Suma = " + s);
        System.out.println("Iloczyn = " + il);
    }
    public static double h(int n){
        Scanner scanner = new Scanner(System.in);
        double a = 0;
        double b = 0;
        for (int i = 0; i < n; i++){
            System.out.println("Podaj liczbe: ");
            b = scanner.nextDouble();
            b = Math.pow(-1,i+1)*b;
            a +=b;
        }
        return a;
    }
    public static double i(int n){
        Scanner scanner = new Scanner(System.in);
        double a = 0;
        double b = 0;
        int silnia = 1;
        for(int i = 0; i < n; i++) {
            silnia *= (i+1);
            System.out.println("Podaj liczbe: ");
            b = scanner.nextDouble();
            b = (pow(-1,i+1)*b)/silnia;
            a += b;
        }
        return a;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ilosc: ");
        int n = scanner.nextInt();

        System.out.println(i(n));

    }

}