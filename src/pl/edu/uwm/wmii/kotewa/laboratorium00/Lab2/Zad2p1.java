package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab2;

import java.util.Scanner;
import static java.lang.Math.*;

public class Zad2p1 {
    public static int a(int n){
        int l = 0;
        for(int i = 0; i < n; i++){
            if(i % 2 !=0){
                l++;
            }
        }
        return l;
    }

    public static int b(int n){
        int l = 0;
        for(int i = 0; i< n; i++){
            if(i%3==0 && i%5 !=0){
                l++;
            }
        }
        return l;
    }

    public static int c(int n){
        int l=0;
        for(double i = 0; i<n; i++){
            double a = Math.pow(i,(1/2));
            if(a%2==0){
                l++;
            }
        }
        return l;
    }

    public static int d(int n){
        int l=0;
        int k;
        for(int i = 1; i<n; i++){
            k = 1;
            if(k < ((i-1)+(i+1))){
                l++;
            }
        }
        return l;
    }



    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę: ");
        int n;
        n = scanner.nextInt();
        int l = a(n);
        System.out.println(l);

    }
}
