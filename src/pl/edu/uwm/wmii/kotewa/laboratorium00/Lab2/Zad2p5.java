package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab2;

import java.util.Scanner;

public class Zad2p5 {
    public static void func(int n) {
        Scanner scanner = new Scanner(System.in);
        float[] wczytane = new float[n];

        for (int i = 0; i < n; i++) {
            System.out.println("Podaj Liczbe: ");
            wczytane[i] = scanner.nextFloat();
        }
        int ilosc = 0;


        for(int i = 0; i < n - 1; i++){
            if (wczytane[i] > 0 && wczytane[i+1] > 0 ){
                ilosc++;
            }

        }
        System.out.println("ilosc: " + ilosc);
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("podaj ilosc liczb: ");
        int n = scanner.nextInt();
        func(n);
    }
}
