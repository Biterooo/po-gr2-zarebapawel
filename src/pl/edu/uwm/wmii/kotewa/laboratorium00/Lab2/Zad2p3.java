package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab2;
import java.util.Scanner;
import static java.lang.Math.*;
public class Zad2p3 {
    public static void func(int n){
        Scanner scanner = new Scanner(System.in);
        float[] wczytane = new float[n];

        for(int i = 0; i < n; i++){
            System.out.println("Podaj Liczbe: ");
            wczytane[i] = scanner.nextFloat();

        }
        int dodatnie = 0;
        int ujemne = 0;
        int zera = 0;

        for(int i = 0; i < n; i++){
            if(wczytane[i] > 0){
                dodatnie++;
            } else if(wczytane[i] < 0){
                ujemne++;
            } else{
                zera++;
            }
        }
        System.out.printf("dodatnie: %d, ujemne: %d, zera: %d",dodatnie, ujemne, zera);
    }
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("podaj ilosc liczb: ");
        int n = scanner.nextInt();
        func(n);
    }
}