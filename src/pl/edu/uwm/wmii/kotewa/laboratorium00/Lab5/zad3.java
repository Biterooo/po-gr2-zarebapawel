package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab5;

import java.util.*;

public class zad3 {
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> nowa = new ArrayList<>();

        Collections.sort(a);
        Collections.sort(b);
        int indexA = 0;
        int indexB = 0;

        while(indexA < a.size() && indexB < b.size()){

            if(a.get(indexA) <= b.get(indexB)){
                nowa.add(a.get(indexA));
                indexA++;
            }
            if(b.get(indexB) <= a.get(indexA)){
                nowa.add(b.get(indexB));
                indexB++;
            }
        }
        nowa.clear();
        nowa.addAll(0, a);
        nowa.addAll(a.size(), b);
        Collections.sort(nowa);

        return nowa;
    }

    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 4, 9, 16));
        ArrayList<Integer> b = new ArrayList<>(Arrays.asList(9, 7, 4, 9, 11));
        System.out.println(mergeSorted(a, b));

    }
}
