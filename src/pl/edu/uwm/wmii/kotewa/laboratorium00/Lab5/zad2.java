package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab5;

import java.util.ArrayList;
import java.util.Arrays;

public class zad2 {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> nowa = new ArrayList<>();

        for(int i = 0; i < a.size() || i < b.size(); i++){
            if(i < a.size()){
                nowa.add(a.get(i));
            }
            if(i < b.size()){
                nowa.add(b.get(i));
            }
        }
        return nowa;
    }

    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        ArrayList<Integer> b = new ArrayList<>(Arrays.asList(7, 8, 9, 10));
        System.out.println(merge(a, b));
    }
}
