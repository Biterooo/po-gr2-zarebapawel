package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab5;

import java.util.*;

public class zad5 {
    public static void reverse(ArrayList<Integer> a){
        for(int i = 0; i < a.size()/ 2; i++){
            int temp = a.get(i);
            a.set(i, a.get(a.size() - 1 - i));
            a.set(a.size() - 1 - i, temp);
        }
    }
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 432432, 9, 8, 999 ,44));
        System.out.println(a);
        reverse(a);
        System.out.println(a);

    }
}
