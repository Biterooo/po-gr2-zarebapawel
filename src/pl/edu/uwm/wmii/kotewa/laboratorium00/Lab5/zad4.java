package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab5;

import java.util.*;

public class zad4 {
    public static ArrayList<Integer> reversed(ArrayList<Integer> a){
        ArrayList<Integer> nowa = new ArrayList<>();
        for(int i = 0; i < a.size(); i++) {
            nowa.add(i, a.get(a.size() - 1 - i));
        }
        return nowa;

    }
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 4, 9, 16));
        System.out.println(reversed(a));

    }
}
