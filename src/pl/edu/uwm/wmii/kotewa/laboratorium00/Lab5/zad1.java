package pl.edu.uwm.wmii.kotewa.laboratorium00.Lab5;

import java.util.ArrayList;
import java.util.Arrays;

public class zad1 {
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> nowa = new ArrayList<>();
        nowa.addAll(0, a);
        nowa.addAll(a.size(), b);

        return nowa;
    }

    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        ArrayList<Integer> b = new ArrayList<>(Arrays.asList(7, 8, 9, 10, 11, 12));
        System.out.println(append(a, b));
    }
}
