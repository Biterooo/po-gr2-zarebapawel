package pl.imiajd.zareba.Lab8;

import java.time.LocalDate;

public class Skrzypce extends Instrument {

    public Skrzypce(String producent, LocalDate rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    void dzwiek() {
        System.out.println("*dzwieki Skrzypiec*");

    }
}
