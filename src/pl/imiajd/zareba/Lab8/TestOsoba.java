package pl.imiajd.zareba.Lab8;


import java.time.LocalDate;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];


        ludzie[0] = new Pracownik("Kowalski", new String[] {"Jan", "Maciek"} , LocalDate.of(1999, 11, 20), false, 5000, LocalDate.of(2020, 11, 20));
        ludzie[1] = new Student("Nowak", new String[] {"Małgorzata"} , LocalDate.of(1999, 3, 25), true, "Informatyka", 3.5);
        System.out.println(ludzie[0].getImiona()[0]);
        System.out.println(ludzie[0].getNazwisko());
        System.out.println(ludzie[0].getDataUrodzenia());
        System.out.println(((Pracownik) ludzie[0]).getPobory());
        ludzie[0].isPlec();

        System.out.println();

        System.out.println(ludzie[1].getImiona()[0]);
        System.out.println(ludzie[1].getNazwisko());
        System.out.println(ludzie[1].getDataUrodzenia());
        ludzie[1].isPlec();

        System.out.println(((Student) ludzie[1]).getSredniaOcen());
        ((Student) ludzie[1]).setSredniaOcen(4.2);
        System.out.println(((Student) ludzie[1]).getSredniaOcen());


    }
}


