package pl.imiajd.zareba.Lab10;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String[] args) {
        ArrayList<Student> grupa = new ArrayList<>();

        grupa.add(new Student("Mickiewicz", LocalDate.of(1798, 12, 24), 4.2));
        grupa.add(new Student("Sienkiewicz", LocalDate.of(1846, 5, 5), 3.9));
        grupa.add(new Student("Zaręba", LocalDate.of(1999, 1, 25), 3.4));
        grupa.add(new Student("Mickiewicz", LocalDate.of(1846, 5, 5), 2.9));
        grupa.add(new Student("Mickiewicz", LocalDate.of(1798, 12, 24), 4.8));

        for (Osoba i : grupa) {
            System.out.println(i);
        }
        System.out.println();

        Collections.sort(grupa);

        for (Osoba i : grupa) {
            System.out.println(i);
        }

    }
}
