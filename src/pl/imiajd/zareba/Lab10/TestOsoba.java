package pl.imiajd.zareba.Lab10;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba {
    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<>();

        grupa.add(new Osoba("Mickiewicz", LocalDate.of(1798, 12, 24)));
        grupa.add(new Osoba("Sienkiewicz", LocalDate.of(1846, 5, 5)));
        grupa.add(new Osoba("Zaręba", LocalDate.of(1999, 1, 25)));
        grupa.add(new Osoba("Mickiewicz", LocalDate.of(1846, 5, 5)));
        grupa.add(new Osoba("Mickiewicz", LocalDate.of(1798, 12, 24)));

        for (Osoba i : grupa) {
            System.out.println(i);
        }
        System.out.println();

        Collections.sort(grupa);

        for (Osoba i : grupa) {
            System.out.println(i);
        }
    }
}
